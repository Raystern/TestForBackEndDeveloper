﻿using Api;
using Common;
using Config.Net;
using ConsoleDataSync.Model;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ConsoleDataSync
{
    class Program
    {
        static void Main(string[] args)
        {
            IServiceCollection services = new ServiceCollection();

            Startup startup = new Startup();
            startup.ConfigureServices(services);

            IAppSettings settings = startup.settings;



            var login = new PostLogin(settings.Api.username, settings.Api.password, settings.Api.Url);
            var bearerToken = login.GetBearerToken();

            var getPlatformWellActual = new GetPlatformWellActual(bearerToken, settings.Api.Url);
            var data = getPlatformWellActual.Response();

            var sqlHelper = new SqlHelper(settings.Sql.connection);
            sqlHelper.Add(data);

            Console.ReadLine();
        }
    }
}
