﻿using Microsoft.EntityFrameworkCore;

namespace Data.Entity
{
    public class TestContext : DbContext
    {
        public TestContext(DbContextOptions<TestContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=localhost;Database=TestlDB;Trusted_Connection=True;");
        }

        public DbSet<t_data> t_data { get; set; }
        public DbSet<t_platformwelldata> t_platformwelldata { get; set; }
        public DbSet<t_well> t_well { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<t_data>().ToTable("t_data");
            modelBuilder.Entity<t_platformwelldata>().ToTable("t_platformwelldata");
            modelBuilder.Entity<t_well>().ToTable("t_well");
        }
    }

}
