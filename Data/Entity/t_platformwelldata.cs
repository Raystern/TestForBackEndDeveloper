﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entity
{
    public class t_platformwelldata
    {
        public int f_id { get; set; }

        public int dataId { get; set; }

        public ICollection<t_well> t_well { get; set; }
    }
}
