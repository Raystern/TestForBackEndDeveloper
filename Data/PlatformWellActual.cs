﻿using System.Collections.Generic;

namespace Data
{
    public class PlatformWellActual : BaseData
    {
        public List<Well> well { get; set; }
    }

    public class Well : BaseData
    {
        public string platformId { get; set; }
    }

    public class BaseData
    {
        public string id { get; set; }

        public string uniqueName { get; set; }

        public string latitude { get; set; }

        public string longitude { get; set; }

        public string createdAt { get; set; }

        public string updatedAt { get; set; }
    }
}
